{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "billevent.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "billevent.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{- define "backend.dep" -}}
{{- print "backend-billevent" -}}
{{- end -}}
{{- define "backend.svc" -}}
{{- print "backend-billevent-svc" -}}
{{- end -}}
{{- define "backend-static.svc" -}}
{{- print "backend-billevent-static-svc" -}}
{{- end -}}
{{- define "backend-static.cm" -}}
{{- print "backend-billevent-static-cm" -}}
{{- end -}}
{{- define "backend.ing" -}}
{{- print "backend-billevent-ing" -}}
{{- end -}}

{{- define "frontend.dep" -}}
{{- print "frontend-billevent" -}}
{{- end -}}
{{- define "frontend.svc" -}}
{{- print "frontend-billevent-svc" -}}
{{- end -}}
{{- define "frontend.ing" -}}
{{- print "frontend-billevent-ing" -}}
{{- end -}}

{{- define "secret" -}}
{{- print "billevent-secret" -}}
{{- end -}}

{{- define "postgres-secret" -}}
{{- print "postgres-secret" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "billevent.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "billevent.labels" -}}
helm.sh/chart: {{ include "billevent.chart" . }}
{{ include "billevent.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "billevent.selectorLabels" -}}
app.kubernetes.io/name: {{ include "billevent.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "billevent.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "billevent.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}
